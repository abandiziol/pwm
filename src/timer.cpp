#include "timer.hpp"

using namespace std;

	void Timer::Int_to_bv()
	{
		dummy_counter = int_counter;
	
		for (int i=0; i<=15; i++) {
			if (dummy_counter%2) {
				temp_counter[i] = 1;
			} else {
				temp_counter[i] = 0;
			}

			dummy_counter = dummy_counter/2;
		}

	
	}

	void Timer::Execute()
	{
		cout << "Timer @ " << sc_time_stamp() << endl;
		
		
		if (counting) {

			if (int_counter == PERIOD) {	
				reset.write(1);
				counting = 0;
				int_counter = 0;
				
				cout << "	PERIOD value reached" << endl;	
			} else {
				int_counter = int_counter + 1;
				Int_to_bv();
				counter = temp_counter;

				cout << "	Counter value is " << int_counter << endl;
			}
		} else {
			
			if (waiting) {
				if (start) {
					counting = 1;
					waiting = 0;

					cout << "	Start counting..." << endl;
				} else {
					cout << "	Waiting..." << endl;
				}
			} else {
				reset.write(0);
				waiting = 1;
	
				cout << "	Reset counter...switching to wait state" << endl;
			}
		}
		
		return;
	}
