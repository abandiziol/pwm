#include "interface.hpp"

using namespace std;

	void Interface::Execute()
	{

		if(waiting_for_input) {	
			if(request.read() == 1) {
				waiting_for_input = 0;
				transferring = 1;
				waiting_for_timer = 0;
				pending_request = 0;

				start = 0;

				internal_register = memory[memory_address.read().to_uint()];
			} else {
				waiting_for_input = 1;
				transferring = 0;
				waiting_for_timer = 0;
				pending_request = 0;
	
				start = 0;
			}
		}
	
		if(transferring) {
			data.write(internal_register);
	
			waiting_for_input = 0;
			transferring = 0;
			waiting_for_timer = 1;
			pending_request = 0;

			start = 1;
		}
	
		if(waiting_for_timer) {
			if(reset) {
				if(pending_request) {
					waiting_for_input = 0;
					transferring = 1;
					waiting_for_timer = 0;
					pending_request = 0;
	
					start = 0;
				} else {
					waiting_for_input = 1;
					transferring = 0;
					waiting_for_timer = 0;
					pending_request = 0;
	
					start = 0;
				}
			} else {
				if(request) {
					waiting_for_input = 0;
					transferring = 0;
					waiting_for_timer = 1;
					pending_request = 1;

					internal_register = memory[memory_address.read().to_uint()];
	
					start = 0;
				} else {
					if(pending_request) {

						waiting_for_input = 0;
						transferring = 0;
						waiting_for_timer = 1;
						pending_request = 1;	
		
						start = 0;
					} else {
						waiting_for_input = 0;
						transferring = 0;
						waiting_for_timer = 1;
						pending_request = 0;	
		
						start = 0;
					}
				}
			}
		}
			

		return;
	}
