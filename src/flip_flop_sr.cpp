#include "flip_flop_sr.hpp"

using namespace std;

	void Flip_Flop_SR::Execute()
	{
		if (set) {
			if (reset == 0) {
				output.write("1");
			} else {
				output.write("Z");
			}
		} else {
			if (reset) {
				output.write("0");
			}
		}
		
		return;
	}
