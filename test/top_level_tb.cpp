#include "systemc.h"
#include <iostream>
#include "top_level.hpp"
#include "interface.hpp"
#include "timer.hpp"
#include "comparator.hpp"
#include "flip_flop_sr.hpp"




int sc_main (int argc, char* argv[]) {

	sc_clock clock("clock");
	sc_signal<sc_bv<4> > memory_address;
	sc_signal<bool> reset;
	sc_signal<sc_bv<16> > memory[MEMORY_SIZE];
	sc_signal<bool> start;
	sc_signal<bool> transferring;
	sc_signal<sc_bv<16> > data1;
	sc_signal<sc_bv<16> > data2;
	sc_signal<bool> one_bigger;
	sc_signal<bool> two_bigger;
	sc_signal<sc_lv<1> > output;
	sc_signal<bool> request;
	sc_signal<bool> waiting_for_timer;
	
	Top_Level t1("Top_Level");

	t1.memory_address(memory_address);
	t1.memory[0](memory[0]);
	t1.memory[1](memory[1]);
	t1.memory[2](memory[2]);
	t1.memory[3](memory[3]);
	t1.memory[4](memory[4]);
	t1.memory[5](memory[5]);
	t1.memory[6](memory[6]);
	t1.memory[7](memory[7]);
	t1.memory[8](memory[8]);
	t1.memory[9](memory[9]);
	t1.memory[10](memory[10]);
	t1.memory[11](memory[11]);
	t1.memory[12](memory[12]);
	t1.memory[13](memory[13]);
	t1.memory[14](memory[14]);
	t1.memory[15](memory[15]);
	t1.clock(clock);
	t1.output(output);
	t1.request(request);

	Interface i1("Interface");
	
	i1.memory_address(memory_address);
	i1.reset(reset);
	i1.memory[0](memory[0]);
	i1.memory[1](memory[1]);
	i1.memory[2](memory[2]);
	i1.memory[3](memory[3]);
	i1.memory[4](memory[4]);
	i1.memory[5](memory[5]);
	i1.memory[6](memory[6]);
	i1.memory[7](memory[7]);
	i1.memory[8](memory[8]);
	i1.memory[9](memory[9]);
	i1.memory[10](memory[10]);
	i1.memory[11](memory[11]);
	i1.memory[12](memory[12]);
	i1.memory[13](memory[13]);
	i1.memory[14](memory[14]);
	i1.memory[15](memory[15]);
	i1.start(start);
	i1.transferring(transferring);
	i1.data(data1);
	i1.clock(clock);
	i1.request(request);
	i1.waiting_for_timer(waiting_for_timer);
	
	Timer tim1("Timer");
	
	tim1.clock(clock);
	tim1.start(start);
	tim1.transferring(transferring);
	tim1.counter(data2);
	tim1.reset(reset);
	tim1.waiting_for_timer(waiting_for_timer);

	Comparator c1("Comparator");

	c1.input1(data1);
	c1.input2(data2);
	c1.one_bigger(one_bigger);
	c1.two_bigger(two_bigger);

	Flip_Flop_SR f1("Flip_Flop_SR");

	f1.set(one_bigger);
	f1.reset(two_bigger);
	f1.output(output);

	//Open VCD file
	sc_trace_file *wf = sc_create_vcd_trace_file("PWM");

	//Dump the desired signals
	sc_trace(wf, clock, "clock");
	sc_trace(wf, memory_address, "memory_address");
	sc_trace(wf, reset, "reset");
	sc_trace(wf, memory[0], "memory[0]");
	sc_trace(wf, start, "start");
	sc_trace(wf, transferring, "transferring");
	sc_trace(wf, data1, "data1");
	sc_trace(wf, data2, "data2");
	sc_trace(wf, one_bigger, "one_bigger");
	sc_trace(wf, two_bigger, "two_bigger");
	sc_trace(wf, output, "output");
	sc_trace(wf, request, "request");


	sc_start(SC_ZERO_TIME);
	sc_start(300000, SC_NS);

	sc_close_vcd_trace_file(wf);
	return 0;

}
