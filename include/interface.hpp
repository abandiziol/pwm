#ifndef INTERFACE_HPP
#define INTERFACE_HPP

//The interface is the part between the memory and the PWM module.
//Since it must activate only when it's called, it's an asynchronous moule, thus
//there's no clock at its input. The memory will not pass the data themselves to the
//PWM module, it will pass the memory location where the data are stored.
//The other input of the interface is the reset signal coming from the timer, indicating
//when the square wave generation has come to an end. As outputs, it has the start signal
//for the clock and the data for the comparator to check.

#include <systemc.h>
#include <iostream>

#define MEMORY_SIZE 16

SC_MODULE(Interface){

	//Declaration of the inputs
	sc_in<sc_bv<4> > memory_address;
	sc_in<bool > reset;
	sc_in<sc_bv<16> > memory[MEMORY_SIZE];
	sc_in<bool> request;
	sc_in<bool> clock;

	//Declaration of the outputs
	sc_out<bool> start;
	sc_out<bool > transferring;
	sc_out<sc_bv<16> > data;
	sc_out<bool> waiting_for_timer;

	//Declaration of internal variables
	sc_bv<16> internal_register;
	bool waiting_for_input;
	bool pending_request;

	//Methods Declaration
	void Execute();

	//Constructor
	SC_CTOR(Interface) {
	
		waiting_for_input = 1;

		SC_METHOD(Execute);
			sensitive_pos << clock;
			dont_initialize();
	}	

};//End of the Interface

#endif
