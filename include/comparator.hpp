#ifndef COMPARATOR_HPP
#define COMPARATOR_HPP

//The comparator will receive in input two 16-bit logic vectors, will translate them
//into unsigned integers and decide which one of the two is bigger.

#include <systemc.h>
#include <iostream>

SC_MODULE(Comparator){

	//Declaration of the inputs
	sc_in<sc_bv<16> > input1;
	sc_in<sc_bv<16> > input2;

	//Declaration of the outputs
	sc_out<bool> one_bigger;
	sc_out<bool> two_bigger;
	//Declaration of signals having testing function
	int one;
	int two;

	//Methods Declaration
	void GetIntFromBitVectors();
	void Compare();

	//Constructor
	SC_CTOR(Comparator) {

		SC_METHOD(Compare);
			sensitive << input1 << input2;	//Comparator is not clocked
			dont_initialize();
	}	

};//End of the Comparator Module

#endif
