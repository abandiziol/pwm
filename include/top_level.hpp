#include <systemc.h>

#define MEMORY_SIZE 16

SC_MODULE(Top_Level){

	//Declaration of the outputs
	sc_out<sc_bv<4> > memory_address;
	sc_out<sc_bv<16> > memory[MEMORY_SIZE];
	sc_out<bool> request;

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_lv<1> > output;

	void stimgen();

	SC_CTOR(Top_Level){

		SC_THREAD(stimgen);
		sensitive_pos << clock;	

	}

};
