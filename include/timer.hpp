#ifndef TIMER_HPP
#define TIMER_HPP

//The inputs of the timer are the clock and the start signal.
//When the start signal has a positive edge, then the timer starts counting.
//The timer counts up when there's a positive edge of the clock.
//When the timer reaches the value corresponding to the period of the PWM module,
//the reset output gets high and the timer's counter is reset.

#include <systemc.h>
#include <iostream>

#define PERIOD 65535

SC_MODULE(Timer){

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<bool> start;
	sc_in<bool> transferring;
	sc_in<bool> waiting_for_timer;

	//Declaration of the outputs
	sc_out<sc_bv<16> > counter;
	sc_out<bool> reset;

	//Declaration of signals having testing function
	int int_counter;
	int counting;
	int waiting;
	int dummy_counter;
	sc_bv<16> temp_counter;

	//Methods Declaration
	void Execute();
	void Int_to_bv();

	//Constructor
	SC_CTOR(Timer) {

		SC_METHOD(Execute);
			sensitive_pos << clock;
			dont_initialize();
	}	

};//End of the Timer Module

#endif
