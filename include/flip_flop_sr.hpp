#ifndef FLIP_FLOP_SR_HPP
#define FLIP_FLOP_SR_HPP

//The flip flop set-reset has two inputs, S (set) and R (reset), and one output, Q.
//His input-out table is the following
//
//		S	|	R	|	Q
//	    -----------------------------------------
//		0	|	0	|      DNC
//		0	|	1	|	0
//		1	|	0	|	1
//		1	|	1	|	Z
//
//The 'Z' value for the output signal means "high impedance", whereas the DNC value
//means that the output mantains the same value it assumed the last time.

#include <systemc.h>
#include <iostream>

SC_MODULE(Flip_Flop_SR){

	//Declaration of the inputs
	sc_in<bool> set;
	sc_in<bool> reset;

	//Declaration of the outputs
	sc_out<sc_lv<1> > output;

	//Methods Declaration
	void Execute();

	//Constructor
	SC_CTOR(Flip_Flop_SR) {

		SC_METHOD(Execute);
			sensitive << set << reset;
			dont_initialize();
	}	

};//End of the Flip-Flop SR Module

#endif
